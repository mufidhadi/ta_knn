@extends('template.dashboard')

@section('judul','Data Latih')
@section('konten')
<a href="{{url('latih/create')}}"  class="btn btn-primary">Tambah</a>
<!-- <a href="{{url('siswa')}}"  class="btn btn-primary">Tambah</a> -->
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead class="text-primary">
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Penghasilan Ayah</th>
                <th>Penghasilan Ibu</th>
                <th>Tanggungan</th>
                <th>Nilai Rapor</th>
                <th>KPS</th>
                <th>Beasiswa</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; ?>
            @foreach ($data_latih as $latih)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $latih->dataSiswa->nama_siswa }}</td>
                <td>{{ $latih->penghasilan_ayah }}</td>
                <td>{{ $latih->penghasilan_ibu }}</td>
                <td>{{ $latih->tanggungan }}</td>
                <td>{{ $latih->nilai_raport }}</td>
                <td>{{ $latih->kps }}</td>
                <td>{{ $latih->beasiswa }}</td>
                <td>
                    <form action="{{ url('latih/'.$latih->id_data_latih) }}" method="post">
                        <a href="{{ url('latih/'.$latih->id_data_latih.'/edit') }}" class="btn btn-sm btn-primary">Edit</a>
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection