@extends('template.dashboard')
@section('judul','Ubah Data Latih')
@section('konten')
<form action="{{url('latih/'.$data_latih->id_data_latih)}}" method="post">
    Nama Siswa <input name="" type="text" class="form-control" value="{{$data_latih->dataSiswa->nama_siswa}}" disabled>
    <input type="hidden" name="id_data_siswa" value="{{ $data_latih->id_data_siswa }}">
    <!-- <select name="id_data_siswa" id="id_data_siswa" class="form-control">
        @foreach($data_siswa as $siswa)
        <option value="{{ $siswa->id_data_siswa }}">{{ $siswa->nama_siswa }}</option>
        @endforeach
    </select> -->
    <br>
    Penghasilan Ayah <input name="penghasilan_ayah" type="text" class="form-control" value="{{$data_latih->penghasilan_ayah}}">
    <br>
    Penghasilan Ibu <input name="penghasilan_ibu" type="text" class="form-control" value="{{$data_latih->penghasilan_ibu}}">
    <br>
    Tanggungan Ortu <input name="tanggungan" type="text" class="form-control" value="{{$data_latih->tanggungan}}">
    <br>
    Nilai Rapor <input name="nilai_raport" type="text" class="form-control" value="{{$data_latih->nilai_raport}}">
    <br>
    KPS 
    <select name="kps" id="kps" class="form-control">
        <option value="ya">ya</option>
        <option value="tidak">Tidak</option>
    </select>
    <br>
    Beasiswa
    <select name="beasiswa" id="beasiswa" class="form-control">
        <option value="ya">ya</option>
        <option value="tidak">Tidak</option>
    </select>
    <br>
    <a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <button type="submit" class="btn btn-primary">Selanjutnya</button>
</form>
@endsection