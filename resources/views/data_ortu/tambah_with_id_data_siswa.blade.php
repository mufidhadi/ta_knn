@extends('template.dashboard')
@section('judul','Tambah Data Orangtua')
@section('konten')
<form action="{{url('ortu')}}" method="post">
    Nama Siswa <input type="text" value="{{ $data_siswa->nama_siswa }}" class="form-control" disabled>
    <input type="hidden" name="id_data_siswa" value="{{ $data_siswa->id_data_siswa }}">
    <br>
    Nama Ayah<input name="nama_ayah" type="text" class="form-control">
    <br>
    Pekerjaan Ayah<input name="pekerjaan_ayah" type="text" class="form-control">
    <br>
    Nama Ibu <input name="nama_ibu" type="text" class="form-control">
    <br>
    Pekerjaan Ibu <input name="pekerjaan_ibu" type="text" class="form-control">
    <br>
    <a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <button type="submit" class="btn btn-primary">Selanjutnya</button>
</form>
@endsection