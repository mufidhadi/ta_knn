@extends('template.dashboard')
@section('judul','Data Orangtua Siswa')
@section('konten')
<a href="{{url('ortu/create')}}" class="btn btn-primary">Tambah</a>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Nama Ayah</th>
                <th>Pekerjaan Ayah</th>
                <th>Nama Ibu</th>
                <th>Pekerjaan Ibu</th>
                <th>Alamat</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; ?>
            @foreach ($data_ortu as $ortu)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $ortu->dataSiswa->nama_siswa }}</td>
                <td>{{ $ortu->nama_ayah }}</td>
                <td>{{ $ortu->pekerjaan_ayah }}</td>
                <td>{{ $ortu->nama_ibu }}</td>
                <td>{{ $ortu->pekerjaan_ibu }}</td>
                <td>
                    <form action="{{ url('ortu/'.$ortu->id_data_ortu) }}" method="post">
                        <a href="{{ url('ortu/'.$ortu->id_data_ortu.'/edit') }}" class="btn btn-sm btn-warning">Edit</a>
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection