@extends('template.dashboard')
@section('judul','Ubah Data Orangtua')
@section('konten')
<form action="{{url('ortu/'.$data_ortu->id_data_ortu)}}" method="post">
    Nama Siswa
    <select name="id_data_siswa" id="id_data_siswa" class="form-control">
        @foreach($data_siswa as $siswa)
        <option value="{{ $siswa->id_data_siswa }}">{{ $siswa->nama_siswa }}</option>
        @endforeach
    </select>
    <br>
    Nama Ayah<input name="nama_ayah" type="text" class="form-control" value="{{ $data_ortu->nama_ayah }}">
    <br>
    Pekerjaan Ayah<input name="pekerjaan_ayah" type="text" class="form-control" value="{{ $data_ortu->pekerjaan_ayah }}">
    <br>
    Nama Ibu <input name="nama_ibu" type="text" class="form-control" value="{{ $data_ortu->nama_ibu }}">
    <br>
    Pekerjaan Ibu <input name="pekerjaan_ibu" type="text" class="form-control" value="{{ $data_ortu->pekerjaan_ibu }}">
    <br>
    <a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <button type="submit" class="btn btn-primary">Selanjutnya</button>
</form>
@endsection