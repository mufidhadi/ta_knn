@extends('template.dashboard')
@section('judul','Ubah Data Uji')
@section('konten')
<form action="{{url('uji/'.$data_uji->id_data_uji)}}" method="post">
    Nama Siswa Nama Siswa <input name="" type="text" class="form-control" value="{{$data_uji->dataSiswa->nama_siswa}}" disabled>
    <input type="hidden" name="id_data_siswa" value="{{ $data_uji->id_data_siswa }}">
    <!-- <select name="id_data_siswa" id="id_data_siswa" class="form-control">
        @foreach($data_siswa as $siswa)
        <option value="{{ $siswa->id_data_siswa }}">{{ $siswa->nama_siswa }}</option>
        @endforeach
    </select> -->
    <br>
    Penghasilan Ayah <input name="penghasilan_ayah" type="number" class="form-control" value="{{$data_uji->penghasilan_ayah}}" min="0">
    <br>
    Penghasilan Ibu <input name="penghasilan_ibu" type="number" class="form-control" value="{{$data_uji->penghasilan_ibu}}" min="0">
    <br>
    Tanggungan Ortu <input name="tanggungan" type="number" class="form-control" value="{{$data_uji->tanggungan}}" min="1">
    <br>
    Nilai Rapor <input name="nilai_raport" type="number" class="form-control" value="{{$data_uji->nilai_raport}}" min="0" max="100">
    <br>
    KPS 
    <select name="kps" id="kps" class="form-control">
        <option value="ya">ya</option>
        <option value="tidak">Tidak</option>
    </select>
    <br>
    <a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
    <input type="hidden" name="_method" value="put">
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <button type="submit" class="btn btn-primary">Selanjutnya</button>
</form>
@endsection