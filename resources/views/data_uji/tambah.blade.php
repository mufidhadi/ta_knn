@extends('template.dashboard')
@section('judul','Tambah Data Uji')
@section('konten')
<form action="{{url('uji')}}" method="post">
    Nama Siswa 
    @if($spesifik)
    <input name="" type="text" class="form-control" readonly value="{{ $data_siswa->nama_siswa }}">
    <input name="id_data_siswa" type="hidden" class="form-control" readonly value="{{ $data_siswa->id_data_siswa }}">
    @else
    <select name="id_data_siswa" id="id_data_siswa" class="form-control">
        @foreach($data_siswa as $siswa)
        <option value="{{ $siswa->id_data_siswa }}">{{ $siswa->nama_siswa }}</option>
        @endforeach
    </select>
    @endif
    <br>
    Penghasilan Ayah <input name="penghasilan_ayah" type="number" class="form-control" min="0">
    <br>
    Penghasilan Ibu <input name="penghasilan_ibu" type="number" class="form-control" min="0">
    <br>
    Tanggungan Ortu <input name="tanggungan" type="number" class="form-control" min="1">
    <br>
    Nilai Rapor <input name="nilai_raport" type="number" class="form-control" min="0" max="100">
    <br>
    KPS 
    <select name="kps" id="kps" class="form-control">
        <option value="ya">ya</option>
        <option value="tidak">Tidak</option>
    </select>
    <br>
    <a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <button type="submit" class="btn btn-primary">Selanjutnya</button>
</form>
@endsection