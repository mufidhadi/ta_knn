@extends('template.dashboard')
@section('judul','Data Uji')
@section('konten')
<a href="{{url('uji/create')}}" class="btn btn-primary">Tambah</a>
<!-- <a href="{{url('siswa')}}" class="btn btn-primary">Tambah</a> -->
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Penghasilan Ayah</th>
                <th>Penghasilan Ibu</th>
                <th>Tanggungan</th>
                <th>Nilai Rapor</th>
                <th>KPS</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; ?>
            @foreach ($data_uji as $uji)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $uji->dataSiswa->nama_siswa }}</td>
                <td>{{ $uji->penghasilan_ayah }}</td>
                <td>{{ $uji->penghasilan_ibu }}</td>
                <td>{{ $uji->tanggungan }}</td>
                <td>{{ $uji->nilai_raport }}</td>
                <td>{{ $uji->kps }}</td>
                <td>
                    <form action="{{ url('uji/'.$uji->id_data_uji) }}" method="post">
                        <a href="{{ url('uji/'.$uji->id_data_uji.'/edit') }}" class="btn btn-sm btn-warning">Edit</a>
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection