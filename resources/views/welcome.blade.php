@extends('template.dashboard')

@section('judul','Dashboard')

@section('konten')
    <h1>Selamat datang!</h1>
    <p>
        Selamat datang di sistem pengambilan keputusan penerima beasiswa BSM dengan metode KNN.
    </p>
    <hr>
    <div class="cotainer-fluid">
        <div class="row">
            <div class="col-md-4">
                <h3><i class="material-icons">group</i> Data Siswa</h3>
                <p>
                    {{$jumlah_data_siswa}} Data
                </p>
            </div>
            <div class="col-md-4">
                <h3><i class="material-icons">timeline</i> Data Latih</h3>
                <p>
                    {{$jumlah_data_latih}} Data
                </p>
            </div>
            <div class="col-md-4">
                <h3><i class="material-icons">bar_chart</i> Akurasi Perhitungan</h3>
                <p>
                    <ul>
                        @foreach($akurasi as $ak)
                        <li>Akurasi Jumlah k {{$ak['k']}}: {{$ak['persen_akurasi']}}%</li>
                        @endforeach
                    </ul>
                </p>
            </div>
        </div>
    </div>
@endsection