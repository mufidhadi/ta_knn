
<!doctype html>
<html lang="id">

<head>
    <meta charset="utf-8" />
    <link rel="apple-touch-icon" sizes="76x76" href="{{url('img/apple-icon.png')}}" />
    <link rel="icon" type="image/png" href="{{url('img/favicon.png')}}" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1" />
    <title>KNN</title>
    <meta content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0' name='viewport' />
    <meta name="viewport" content="width=device-width" />
    <!-- Bootstrap core CSS     -->
    <link href="{{url('css/bootstrap.min.css')}}" rel="stylesheet" />
    <!--  Material Dashboard CSS    -->
    <link href="{{url('css/material-dashboard.css?v=1.2.0')}}" rel="stylesheet" />
    <!--     Fonts and icons     -->
    <link href="http://maxcdn.bootstrapcdn.com/font-awesome/latest/css/font-awesome.min.css" rel="stylesheet">
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,700,300|Material+Icons' rel='stylesheet' type='text/css'>
</head>

<body>
    <div class="wrapper">
        <div class="sidebar" data-color="red" data-image="{{url('img/sidebar-1.jpg')}}">
            <!--
                Tip 1: You can change the color of the sidebar using: data-color="purple | blue | green | orange | red"

                Tip 2: you can also add an image using data-image tag
            -->
            <div class="logo">
                <a href="{{url('/')}}" class="simple-text">
                    <i class="material-icons">bar_chart</i>
                    KNN
                </a>
            </div>
            <div class="sidebar-wrapper">
                <ul class="nav">
                    <li @if(url()->current()==url('/')) class="active" @endif>
                        <a href="{{url('/')}}">
                            <i class="material-icons">dashboard</i>
                            <p>Dashboard</p>
                        </a>
                    </li>
                    <li @if(url()->current()==url('/siswa')) class="active" @endif>
                        <a href="{{url('/siswa')}}">
                            <i class="material-icons">group</i>
                            <p>Data Siswa</p>
                        </a>
                    </li>
                    <!-- <li @if(url()->current()==url('/latih')) class="active" @endif>
                        <a href="{{url('/latih')}}">
                            <i class="material-icons">playlist_add_check</i>
                            <p>Data Latih</p>
                        </a>
                    </li>
                    <li @if(url()->current()==url('/uji')) class="active" @endif>
                        <a href="{{url('/uji')}}">
                            <i class="material-icons">content_paste</i>
                            <p>Data Uji</p>
                        </a>
                    </li> -->
                    <li @if(url()->current()==url('/hitung/latih')) class="active" @endif>
                        <a href="{{url('/hitung/latih')}}">
                            <i class="material-icons">timeline</i>
                            <p>Hitung Akurasi</p>
                        </a>
                    </li>
                    <li @if(url()->current()==url('/hitung/uji')) class="active" @endif>
                        <a href="{{url('/hitung/uji')}}">
                            <i class="material-icons">timeline</i>
                            <p>Hitung Keputusan</p>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="main-panel">
            <nav class="navbar navbar-transparent navbar-absolute">
                <div class="container-fluid">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="navbar-brand" href="#"> @yield('judul') </a>
                    </div>
                    <div class="collapse navbar-collapse">
                        <ul class="nav navbar-nav navbar-right">
                            <li>
                                <!-- <a href="#pablo" class="dropdown-toggle" data-toggle="dropdown">
                                    <i class="material-icons">person</i>
                                    <p class="hidden-lg hidden-md">Profile</p>
                                </a> -->
                            </li>
                        </ul>
                    </div>
                </div>
            </nav>
            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="card">
                                <div class="card-header" data-background-color="purple">
                                    <h4 class="title">@yield('judul')</h4>
                                    <p class="category">@yield('sub_judul')</p>
                                </div>
                                <div class="card-content">
                                    <!-- tempat konten utama -->
                                    @yield('konten')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <footer class="footer">
                <div class="container-fluid">
                    <p class="copyright pull-right">
                        &copy;
                        <script>
                            document.write(new Date().getFullYear())
                        </script>
                        <a href="http://www.creative-tim.com">Mufid Hadi</a>, Tugas Akir Informatika UTY
                    </p>
                </div>
            </footer>
        </div>
    </div>
</body>
<!--   Core JS Files   -->
<script src="{{url('js/jquery-3.2.1.min.js')}}" type="text/javascript"></script>
<script src="{{url('js/bootstrap.min.js')}}" type="text/javascript"></script>
<script src="{{url('js/material.min.js')}}" type="text/javascript"></script>
<!--  PerfectScrollbar Library -->
<script src="{{url('js/perfect-scrollbar.jquery.min.js')}}"></script>
<!-- Material Dashboard javascript methods -->
<script src="{{url('js/material-dashboard.js?v=1.2.0')}}"></script>

</html>