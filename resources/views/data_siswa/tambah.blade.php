@extends('template.dashboard')
@section('judul','Tambah Data Siswa')
@section('konten')
<form action="{{url('siswa')}}" method="post">
    Nama Siswa <input name="nama_siswa" type="text" class="form-control">
    <br>
    Kelas <input name="kelas" type="number" class="form-control" min="1">
    <br>
    Jenis Kelamin 
    <select name="jenis_kelamin" id="jenis_kelamin" class="form-control">
        <option value="laki-laki">Laki-laki</option>
        <option value="perempuan">Perempuan</option>
    </select>
    <br>
    Tempat Lahir<input name="tempat_lahir" type="text" class="form-control">
    <br>
    Tanggal Lahir<input name="tanggal_lahir" type="date" class="form-control">
    <br>
    Alamat <textarea name="alamat" id="alamat" cols="30" rows="3" class="form-control"></textarea>
    <br>
    <a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <button type="submit" class="btn btn-primary">Selanjutnya</button>
</form>
@endsection