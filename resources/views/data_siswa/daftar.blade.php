@extends('template.dashboard')
@section('judul','Data Siswa')
@section('konten')
<a href="{{url('siswa/create')}}" class="btn btn-primary">Tambah</a>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Jenis Kelamin</th>
                <th>TTL</th>
                <th>Nama Ayah</th>
                <th>Pekerjaan Ayah</th>
                <th>Nama Ibu</th>
                <th>Pekerjaan Ibu</th>
                <th>Alamat</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; ?>
            @foreach ($data_siswa as $siswa)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $siswa->nama_siswa }}</td>
                <td>{{ $siswa->kelas }}</td>
                <td>{{ $siswa->jenis_kelamin }}</td>
                <td>
                    {{ $siswa->tempat_lahir }},
                    {{ $siswa->tanggal_lahir }}
                </td>
                <td>{{@$siswa->dataOrtu->nama_ayah}}</td>
                <td>{{@$siswa->dataOrtu->pekerjaan_ayah}}</td>
                <td>{{@$siswa->dataOrtu->nama_ibu}}</td>
                <td>{{@$siswa->dataOrtu->pekerjaan_ibu}}</td>
                <td>{{ $siswa->alamat }}</td>
                <td>
                    <form action="{{ url('siswa/'.$siswa->id_data_siswa) }}" method="post" class="btn-group-sm btn-group-vertical">
                        <a href="{{ url('uji/create?siswa='.$siswa->id_data_siswa) }}" class="btn btn-xs btn-primary"><i class="material-icons">add</i> uji</a>
                        <a href="{{ url('latih/create?siswa='.$siswa->id_data_siswa) }}" class="btn btn-xs btn-primary"><i class="material-icons">add</i> latih</a>
                        <a href="{{ url('siswa/'.$siswa->id_data_siswa.'/edit') }}" class="btn btn-sm btn-warning">Edit</a>
                        <input type="hidden" name="_method" value="delete">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-sm btn-danger">Hapus</button>
                    </form>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection