@extends('template.dashboard')
@section('judul','Detail Hasil Hitung Data KNN')
@section('konten')
<h4>Data Latih</h4>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead class="text-primary">
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Pekerjaan Ayah</th>
                <th>Penghasilan Ayah</th>
                <th>Pekerjaan Ibu</th>
                <th>Penghasilan Ibu</th>
                <th>Tanggungan</th>
                <th>Nilai Rapor</th>
                <th>KPS</th>
                <th>Beasiswa</th>
                <th>Jarak Euclidean</th>
                <th>Urutan</th>
                <th>Anggota K</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; ?>
            @foreach ($detail_hitung as $dh)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{$dh->dataLatih->dataSiswa->nama_siswa}}</td>
                <td>{{$dh->dataLatih->dataSiswa->dataOrtu->pekerjaan_ayah}}</td>
                <td>{{ $dh->dataLatih->penghasilan_ayah }}</td>
                <td>{{$dh->dataLatih->dataSiswa->dataOrtu->pekerjaan_ibu}}</td>
                <td>{{ $dh->dataLatih->penghasilan_ibu }}</td>
                <td>{{ $dh->dataLatih->tanggungan }}</td>
                <td>{{ $dh->dataLatih->nilai_raport }}</td>
                <td>{{ $dh->dataLatih->kps }}</td>
                <td>{{ $dh->dataLatih->beasiswa }}</td>
                <td>{{ $dh->jarak_euclidean }}</td>
                <td>{{ $dh->urutan }}</td>
                <td>{{ $dh->anggota_k }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<h4>Data Uji</h4>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead class="text-primary">
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Pekerjaan Ayah</th>
                <th>Penghasilan Ayah</th>
                <th>Pekerjaan Ibu</th>
                <th>Penghasilan Ibu</th>
                <th>Tanggungan</th>
                <th>Nilai Rapor</th>
                <th>KPS</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; ?>
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $data_uji->dataSiswa->nama_siswa }}</td>
                <td>{{ $data_uji->dataSiswa->dataOrtu->pekerjaan_ayah }}</td>
                <td>{{ $data_uji->penghasilan_ayah }}</td>
                <td>{{ $data_uji->dataSiswa->dataOrtu->pekerjaan_ibu }}</td>
                <td>{{ $data_uji->penghasilan_ibu }}</td>
                <td>{{ $data_uji->tanggungan }}</td>
                <td>{{ $data_uji->nilai_raport }}</td>
                <td>{{ $data_uji->kps }}</td>
            </tr>
        </tbody>
    </table>
</div>

<h4>Kesimpulan</h4>
<ul>
    <li>Jumlah K : {{$hitung->jumlah_k}}</li>
    <li>Persentase penerima beasiswa anggota K : {{number_format((float) $jumlah_beasiswa_k/$hitung->jumlah_k*100, 2, '.', '')}}%</li>
    <li>Data uji termasuk penerima beasiswa : {{$hitung->kesimpulan_beasiswa}}</li>
</ul>
<form action="{{url('add_to_latih/'.$hitung->id_hitung)}}" method="post">
    <a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
    <input type="hidden" name="_token" value="{{csrf_token()}}">
    <button type="submit" class="btn btn-primary">Simpan ke data latih</button>
</form>
@endsection