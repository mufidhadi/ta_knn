@extends('template.dashboard')
@section('judul','Data Uji')
@section('konten')
<a href="{{url('uji/create')}}" class="btn btn-primary">Tambah data uji</a>
<div class="table-responsive">
</div>
<table class="table table-sriped table-bordered table-hover">
    <thead>
        <tr>
            <th>No</th>
            <th>Nama Siswa</th>
            <th>Penghasilan Ayah</th>
            <th>Penghasilan Ibu</th>
            <th>Tanggungan</th>
            <th>Nilai Rapor</th>
            <th>KPS</th>
            <th>Hitung</th>
        </tr>
    </thead>
    <tbody>
        <?php $no = 0; ?>
        @foreach ($data_uji as $uji)
        <tr>
            <td>{{ ++$no }}</td>
            <td>{{ $uji->dataSiswa->nama_siswa }}</td>
            <td>{{ $uji->penghasilan_ayah }}</td>
            <td>{{ $uji->penghasilan_ibu }}</td>
            <td>{{ $uji->tanggungan }}</td>
            <td>{{ $uji->nilai_raport }}</td>
            <td>{{ $uji->kps }}</td>
            <td class="container-fluid">
                <form action="{{ url('hitung') }}" method="post" class="row">
                    <div class="col-xs-8">
                        <input type="number" name="jumlah_k" min="1" placeholder="inputkan jumlah K" class="form-control">
                    </div>
                    <div class="col-xs-4">
                        <input type="hidden" name="id_data_uji" value="{{$uji->id_data_uji}}">
                        <input type="hidden" name="_token" value="{{csrf_token()}}">
                        <button type="submit" class="btn btn-sm btn-primary">Hitung</button>
                    </div>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>
<a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
@endsection