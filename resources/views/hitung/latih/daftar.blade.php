@extends('template.dashboard')
@section('judul','Hasil Hitung Data KNN')
@section('konten')
<a href="{{url('hitung/latih/create')}}" class="btn btn-primary">Tambah</a>
@foreach($group as $gr)
<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <br>
            <div class="table-responsive">
                <table class="table striped table-bordered table-hover">
                    <thead>
                        <tr>
                            <th>No</th>
                            <th>Nama Siswa</th>
                            <th>Jumlah K</th>
                            <th>Beasiswa Berdasar Data Asli</th>
                            <th>Beasiswa Berdasar Hasil KNN</th>
                            <th>Aksi</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php $no = 0; ?>
                        @foreach ($gr['hitung'] as $htg)
                        <tr>
                            <td>{{ ++$no }}</td>
                            <td>{{ $htg->nama_siswa }}</td>
                            <td>{{ $htg->jumlah_k }}</td>
                            <td>{{ $htg->beasiswa }}</td>
                            <td>
                                {{ $htg->kesimpulan_beasiswa }}
                                @if($htg->beasiswa == $htg->kesimpulan_beasiswa)
                                <span class="label label-success">Sesuai</span>
                                @endif
                            </td>
                            <td>
                                <a href="{{ url('hitung/latih/'.$htg->id_hitung.'') }}" class="btn btn-sm btn-default">Detail</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
            <ul>
                <li>
                    Perbandingan akurasi untuk jumlah K {{$gr['k']}} : <b>{{$gr['jumlah_sama']}}</b>/{{$gr['jumlah_hitung']}}
                    (jumlah sesuai/jumlah hitung)
                </li>
                <li>Akurasi untuk jumlah K {{$gr['k']}} (dalam persen): <b>{{$gr['persen_akurasi']}}</b>%</li>
            </ul>
        </div>
    </div>
</div>

@endforeach
@endsection