@extends('template.dashboard')
@section('judul','Detail Hasil Hitung Data KNN')
@section('konten')
<h4>Data Latih</h4>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead class="text-primary">
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Pekerjaan Ayah</th>
                <th>Penghasilan Ayah</th>
                <th>Pekerjaan Ibu</th>
                <th>Penghasilan Ibu</th>
                <th>Tanggungan</th>
                <th>Nilai Rapor</th>
                <th>KPS</th>
                <th>Beasiswa</th>
                <th>Jarak Euclidean</th>
                <!-- <th>Urutan</th> -->
                <th>Anggota K</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; ?>
            @foreach ($detail_hitung as $dh)
            <tr @if($dh->anggota_k=='ya') style="background-color: lightgreen;" @endif >
                <td>{{ ++$no }}</td>
                <td>{{$dh->dataLatih->dataSiswa->nama_siswa}}</td>
                <td>{{$dh->dataLatih->dataSiswa->dataOrtu->pekerjaan_ayah}}</td>
                <td>{{ $dh->dataLatih->penghasilan_ayah }}</td>
                <td>{{$dh->dataLatih->dataSiswa->dataOrtu->pekerjaan_ibu}}</td>
                <td>{{ $dh->dataLatih->penghasilan_ibu }}</td>
                <td>{{ $dh->dataLatih->tanggungan }}</td>
                <td>{{ $dh->dataLatih->nilai_raport }}</td>
                <td>{{ $dh->dataLatih->kps }}</td>
                <td>{{ $dh->dataLatih->beasiswa }}</td>
                <td>{{ $dh->jarak_euclidean }}</td>
                <!-- <td>{{ $dh->urutan }}</td> -->
                <td>{{ $dh->anggota_k }}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>

<h4>Data yang Diujikan</h4>
<div class="table-responsive">
    <table class="table table-striped table-bordered table-hover">
        <thead class="text-primary">
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Pekerjaan Ayah</th>
                <th>Penghasilan Ayah</th>
                <th>Pekerjaan Ibu</th>
                <th>Penghasilan Ibu</th>
                <th>Tanggungan</th>
                <th>Nilai Rapor</th>
                <th>KPS</th>
                <th>Beasiswa</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; ?>
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $data_latih->dataSiswa->nama_siswa }}</td>
                <td>{{ $data_latih->dataSiswa->dataOrtu->pekerjaan_ayah }}</td>
                <td>{{ $data_latih->penghasilan_ayah }}</td>
                <td>{{ $data_latih->dataSiswa->dataOrtu->pekerjaan_ibu }}</td>
                <td>{{ $data_latih->penghasilan_ibu }}</td>
                <td>{{ $data_latih->tanggungan }}</td>
                <td>{{ $data_latih->nilai_raport }}</td>
                <td>{{ $data_latih->kps }}</td>
                <td>{{ $data_latih->beasiswa }}</td>
            </tr>
        </tbody>
    </table>
</div>

<h4>Kesimpulan</h4>
<ul>
    <li>Jumlah K : {{$hitung->jumlah_k}}</li>
    @if($hitung->kesimpulan_beasiswa=='ya')
    <li>Persentase penerima beasiswa dari anggota K : {{number_format((float) $jumlah_beasiswa_k/$hitung->jumlah_k*100, 2, '.', '')}}%</li>
    @else
    <li>Persentase bukan penerima beasiswa dari anggota K : {{number_format((float) $jumlah_tidak_beasiswa_k/$hitung->jumlah_k*100, 2, '.', '')}}%</li>
    @endif
    <li>Data yang diujikan termasuk penerima beasiswa (data awal) : {{$data_latih->beasiswa}}</li>
    <li>
        Data yang diujikan termasuk penerima beasiswa <b>(hasil perhitungan)</b> : {{$hitung->kesimpulan_beasiswa}}
        @if($sesuai)
            <span class="label label-success">Sesuai data awal</span>
        @else
            <span class="label label-danger">Tidak Sesuai data awal</span>
        @endif
    </li>
</ul>
<a href="{{url()->previous()}}" class="btn btn-default">Kembali</a>
@endsection