@extends('template.dashboard')
@section('judul','Hasil Hitung Data KNN')
@section('konten')
<a href="{{url('hitung/uji/create')}}" class="btn btn-primary">Tambah</a>
<div class="table-responsive">
    <table class="table striped table-bordered table-hover">
        <thead>
            <tr>
                <th>No</th>
                <th>Nama Siswa</th>
                <th>Jumlah K</th>
                <th>Kesimpulan Beasiswa</th>
                <th>Aksi</th>
            </tr>
        </thead>
        <tbody>
            <?php $no = 0; ?>
            @foreach ($hitung as $htg)
            <tr>
                <td>{{ ++$no }}</td>
                <td>{{ $htg->nama_siswa }}</td>
                <td>{{ $htg->jumlah_k }}</td>
                <td>{{ $htg->kesimpulan_beasiswa }}</td>
                <td>
                    <a href="{{ url('hitung/uji/'.$htg->id_hitung.'') }}" class="btn btn-sm btn-default">Detail</a>
                </td>
            </tr>
            @endforeach
        </tbody>
    </table>
</div>
@endsection