<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dataSiswa extends Model
{
    protected $table = 'data_siswa';
    protected $primaryKey = 'id_data_siswa';
    public $timestamps = false;

    public function dataOrtu(){
        return $this->belongsTo('App\dataOrtu', 'id_data_siswa', 'id_data_siswa');
    }

    public function hitung()
    {
        return $this->hasManyThrough(
            'App\hitung',
            'App\data_uji',
            'id_hitung', // Foreign key on users table...
            'id_data_uji', // Foreign key on posts table...
            'id_data_siswa', // Local key on countries table...
            'id_data_uji' // Local key on users table...
        );
    }
}
