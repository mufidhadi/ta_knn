<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;
use App\DataSiswa;
use App\Data_latih;
use App\Hitung;
use App\Detail_hitung;

class HomeController extends Controller
{
    public function index()
    {
        $data_siswa = DataSiswa::all();
        $data['jumlah_data_siswa'] = count($data_siswa);
        $data_latih = Data_latih::all();
        $data['jumlah_data_latih'] = count($data_latih);
        $data['akurasi'] = $this->akurasi();
        return view('welcome',$data);
    }

    public function akurasi()
    {
        $data  = array();
        $hitung_group = Hitung::all();
        $k = array(0);
        foreach ($hitung_group as $group) {
            if (!(array_search($group->jumlah_k,$k))) {
                array_push($k,$group->jumlah_k);
            }
        }
        foreach ($k as $jumlah_k) {
            if ($jumlah_k!=0) {
                $hitung = DB::table('hitung')
                        ->join('data_latih', 'hitung.id_data_latih', '=', 'data_latih.id_data_latih')
                        ->join('data_siswa', 'data_latih.id_data_siswa', '=', 'data_siswa.id_data_siswa')
                        ->where('jumlah_k',$jumlah_k)
                        ->get();
                $jumlah_sama = 0;
                foreach ($hitung as $htg ) {
                    if($htg->beasiswa == $htg->kesimpulan_beasiswa)
                        $jumlah_sama++;
                }
                $jumlah_hitung = count($hitung);
                $jumlah_hitung = ($jumlah_hitung<=0) ? 1 : $jumlah_hitung ;
                $akurasi = $jumlah_sama / $jumlah_hitung;
                $persen_akurasi = $akurasi * 100;
                $akurasi = number_format((float)$akurasi, 2, '.', '');
                $persen_akurasi = number_format((float)$persen_akurasi, 2, '.', '');
                array_push($data,[
                    'k' => $jumlah_k,
                    'jumlah_sama' => $jumlah_sama,
                    'jumlah_hitung' => $jumlah_hitung,
                    'akurasi' => $akurasi,
                    'persen_akurasi' => $persen_akurasi,
                ]);
            }
        }
        return $data;
    }
}
