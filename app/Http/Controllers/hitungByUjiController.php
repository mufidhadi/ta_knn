<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Http\Request;
use App\hitung;
use App\detail_hitung;
use App\data_uji;
use App\data_latih;
// (y) 1. Menentukan parameter k (jumlah tetangga paling dekat)
// (y) 2. Menghitung kuadrat jarak eucliden objek terhadap data training yang diberikan
// (y) 3. Mengurutkan hasil no 2 secara ascending 
// (y) 4. Mengumpulkan kategori Y (Klasifikasi nearest neighbor berdasarkan nilai k) 
// (y) 5. Dengan menggunakan kategori nearest neighbor yang paling mayoritas maka dapat dipredisikan kategori objek 

class hitungByUjiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $hitung = DB::table('hitung')
                ->join('data_uji', 'hitung.id_data_uji', '=', 'data_uji.id_data_uji')
                ->join('data_siswa', 'data_uji.id_data_siswa', '=', 'data_siswa.id_data_siswa')
                ->get();
        // $hitung = hitung::all();
        $data['hitung'] = $hitung;
        return view('hitung.uji.daftar',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data_uji = data_uji::all();
        $data['data_uji'] = $data_uji;
        return view('hitung.uji.daftar_uji', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // menentukan K
        $hitung = new hitung;
        $hitung->id_data_uji = $request->id_data_uji;
        $hitung->jumlah_k = $request->jumlah_k;
        $hitung->kesimpulan_beasiswa = null;
        $hitung->save();
        // perhitungan KNN
        $id_hitung = $hitung->id_hitung;
        $this->euclidean_distance($id_hitung);
        $this->order_k($id_hitung);
        $this->conclution($id_hitung);
        return redirect('hitung/uji/'.$id_hitung)->with('pesan','perhitungan selesai');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $detail_hitung = detail_hitung::where('id_hitung',$id)->orderBy('urutan', 'asc')->get();
        $hitung = hitung::find($id);
        $data_uji = data_uji::find($hitung->id_data_uji);
        $jumlah_beasiswa_k = 0;
        $jumlah_tidak_beasiswa_k = 0;
        foreach ($detail_hitung as $dh) {
            if ($dh->anggota_k=='ya') {
                if ($dh->dataLatih->beasiswa=='ya') {
                    $jumlah_beasiswa_k++;
                } else {
                    $jumlah_tidak_beasiswa_k++;
                }
            }
        }
        $data['detail_hitung'] = $detail_hitung;
        $data['hitung'] = $hitung;
        $data['data_uji'] = $data_uji;
        $data['jumlah_beasiswa_k'] = $jumlah_beasiswa_k;
        $data['jumlah_tidak_beasiswa_k'] = $jumlah_tidak_beasiswa_k;
        
        return view('hitung.uji.daftar_detail',$data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public function euclidean_distance($id_hitung)
    {
        // echo "<pre>";
        $hitung = hitung::find($id_hitung);
        $id_data_uji = $hitung->id_data_uji;
        $data_uji = data_uji::find($id_data_uji);
        $data_latih = data_latih::all();
        foreach ($data_latih as $dt_latih) {
            $id_data_latih = $dt_latih->id_data_latih;
            $kolom_data_uji = Schema::getColumnListing('data_uji');
            $eucliedan_baris = 0;
            foreach ($kolom_data_uji as $key) {
                if ($key != 'id_data_uji' && $key != 'id_data_siswa' && $key != 'kps') {
                    //perhitungan
                    // echo 'uji->'.$key.'<br>';
                    // echo 'dt_latih->'.$key.'<br>';
                    $eucliedan_kolom = 0;
                    $latih = $dt_latih->$key;
                    $uji = $data_uji->$key;
                    // $eucliedan_kolom = ($latih - $uji) * ($latih - $uji);
                    $eucliedan_kolom = ($latih - $uji);
                    $eucliedan_kolom *= $eucliedan_kolom;
                    // echo "euclidean = ($latih - $uji)^2 <br>";
                    $eucliedan_baris += $eucliedan_kolom;
                }
            }
            $eucliedan_baris = sqrt($eucliedan_baris);
            // echo "<b>sqrt euclidean baris = $eucliedan_baris</b>";
            // echo "<hr>";
            // $insert_detail_hitung = $this->detail_hitung_model->insert_detail_hitung($id_hitung, $id_data_latih, $eucliedan_baris, '', '');
            // if ($insert_detail_hitung) {
            //     return true;
            // } else {
            //     return false;
            // }
            $detail_hitung = new detail_hitung;
            $detail_hitung->id_hitung = $id_hitung;
            $detail_hitung->id_data_latih = $id_data_latih;
            $detail_hitung->jarak_euclidean = $eucliedan_baris;
            $detail_hitung->save();
        }
    }

    public function order_k($id_hitung)
    {
        // echo "<pre>";
        $hitung = hitung::find($id_hitung);
        $jumlah_k = $hitung->jumlah_k;
        $detail_hitung = detail_hitung::where('id_hitung',$id_hitung)->orderBy('jarak_euclidean', 'asc')->get();
        // var_dump($detail_hitung);
        // echo "<hr>";
        $urutan = 1;
        foreach ($detail_hitung as $detail) {
            $id_detail_hitung = $detail->id_detail_hitung;
            $id_data_latih = $detail->id_data_latih;
            $jarak_euclidean = $detail->jarak_euclidean;
            if ($urutan <= $jumlah_k) {
                $anggota_k = 'ya';
            }else {
                $anggota_k = 'tidak';
            }
            // update
            // $update = $this->detail_hitung_model->update_detail_hitung($id_detail_hitung, $id_hitung, $id_data_latih, $jarak, $urutan, $anggota_k);
            $update_detail_hitung = detail_hitung::find($id_detail_hitung);
            $update_detail_hitung->urutan = $urutan;
            $update_detail_hitung->anggota_k = $anggota_k;
            $update_detail_hitung->save();
            // echo "id_detail_hitung: $id_detail_hitung | urutan : $urutan <br>";
            $urutan++;
        }
        // if ($update) {
        // return true;
        // } else {
        // return false;
        // }
    }

    public function conclution($id_hitung)
    {
        $jumlah_beasiswa = array(
            'ya' => 0,
            'tidak' => 0
        );
        $detail_hitung = detail_hitung::where('id_hitung',$id_hitung)->orderBy('jarak_euclidean', 'asc')->get();
        foreach ($detail_hitung as $detail) {
            $anggota_k = $detail->anggota_k;
            if ($anggota_k == 'ya') {
                $id_data_latih = $detail->id_data_latih;
                $data_latih = data_latih::find($id_data_latih);
                $beasiswa = $data_latih->beasiswa;
                foreach ($jumlah_beasiswa as $key => $value){
                    if ($beasiswa == $key) {
                        $jumlah_beasiswa[$key]+=1;
                    }
                }
            }
        }
        // echo "<pre>";
        // var_dump($jumlah_beasiswa);
        $kesimpulan = array_search(max($jumlah_beasiswa), $jumlah_beasiswa);
        // echo "<br>kesimpulan : $kesimpulan";
        // update hitung
        $hitung = hitung::find($id_hitung);
        $hitung->kesimpulan_beasiswa = $kesimpulan;
        $hitung->save();
    }

    public function add_to_latih($id_hitung)
    {
        $hitung = hitung::find($id_hitung);
        $id_data_uji = $hitung->id_data_uji;
        $data_uji = data_uji::find($id_data_uji);
        $data_latih = new data_latih;
        $data_latih->id_data_siswa = $data_uji->id_data_siswa;
        $data_latih->penghasilan_ayah = $data_uji->penghasilan_ayah;
        $data_latih->penghasilan_ibu = $data_uji->penghasilan_ibu;
        $data_latih->tanggungan = $data_uji->tanggungan;
        $data_latih->nilai_raport = $data_uji->nilai_raport;
        $data_latih->kps = $data_uji->kps;
        $data_latih->beasiswa = $hitung->kesimpulan_beasiswa;
        $data_latih->save();
        return redirect('hitung/uji/')->with('pesan','tersimpan');
    }
}
