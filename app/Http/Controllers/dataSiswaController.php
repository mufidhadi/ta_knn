<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dataSiswa;
use App\dataOrtu;

class dataSiswaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_siswa = dataSiswa::all();
        $data['data_siswa'] = $data_siswa;
        return view('data_siswa.daftar', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('data_siswa.tambah');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo 'yes';
        // validasi
        // $rules = array(
        //         'nama' => 'required',
        //         'harga' => 'numeric|min:1000',
        //         );
        // $validatedData = $request->validate($rules);

        // input ke database
        $data_siswa = new dataSiswa;
        $data_siswa->nama_siswa = $request->nama_siswa;
        $data_siswa->kelas = $request->kelas;
        $data_siswa->jenis_kelamin = $request->jenis_kelamin;
        $data_siswa->tempat_lahir = $request->tempat_lahir;
        $data_siswa->tanggal_lahir = $request->tanggal_lahir;
        $data_siswa->alamat = $request->alamat;
        $data_siswa->save();
        echo "tersimpan";
        // redirect ke /ortu/create
        return redirect('ortu/create/'.$data_siswa->id_data_siswa)->with('pesan','tersimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_siswa = dataSiswa::find($id);
        // $data_siswa = dataSiswa::where('id_data_siswa',$id)->first();
        $data['data_siswa'] = $data_siswa;
        return view('data_siswa.ubah', $data);
        //
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_siswa = dataSiswa::find($id);
        $data_siswa->nama_siswa = $request->nama_siswa;
        $data_siswa->kelas = $request->kelas;
        $data_siswa->jenis_kelamin = $request->jenis_kelamin;
        $data_siswa->tempat_lahir = $request->tempat_lahir;
        $data_siswa->tanggal_lahir = $request->tanggal_lahir;
        $data_siswa->alamat = $request->alamat;
        $data_siswa->save();
        echo "tersimpan";
        // redirect ke /ortu/{id}/edit
        $data_ortu = dataOrtu::where('id_data_siswa',$id)->first();
        return redirect('ortu/'.$data_ortu->id_data_ortu.'/edit_siswa/')->with('pesan','tersimpan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_ortu = dataOrtu::where('id_data_siswa',$id)->first();
        $data_ortu->delete();
        $data_siswa = dataSiswa::find($id);
        $data_siswa->delete();
        echo "terhapus";
        return redirect('siswa')->with('pesan','tersimpan');
    }
}
