<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\dataOrtu;
use App\dataSiswa;

class dataOrtuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_ortu = dataOrtu::all();
        $data['data_ortu'] = $data_ortu;
        return view('data_ortu.daftar', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data_siswa = dataSiswa::all();
        $data['data_siswa'] = $data_siswa;
        return view('data_ortu.tambah',$data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create_with_id_data_siswa($id_data_siswa)
    {
        $data_siswa = dataSiswa::find($id_data_siswa);
        // $data_siswa = dataSiswa::where('id_data_siswa',$id_data_siswa)->first();
        $data['data_siswa'] = $data_siswa;
        return view('data_ortu.tambah_with_id_data_siswa',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        echo 'yes';
        // validasi
        // $rules = array(
        //         'nama' => 'required',
        //         'harga' => 'numeric|min:1000',
        //         );
        // $validatedData = $request->validate($rules);

        // input ke database
        $data_ortu = new dataOrtu;
        $data_ortu->id_data_siswa = $request->id_data_siswa;
        $data_ortu->nama_ayah = $request->nama_ayah;
        $data_ortu->pekerjaan_ayah = $request->pekerjaan_ayah;
        $data_ortu->nama_ibu = $request->nama_ibu;
        $data_ortu->pekerjaan_ibu = $request->pekerjaan_ibu;
        $data_ortu->save();
        echo "tersimpan";
        // redirect ke /ortu/create
        return redirect('/siswa')->with('pesan','tersimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_ortu = dataOrtu::find($id);
        // $data_ortu = dataOrtu::where('id_data_ortu',$id)->first();
        $data['data_ortu'] = $data_ortu;
        $data_siswa = dataSiswa::all();
        $data['data_siswa'] = $data_siswa;
        return view('data_ortu.ubah', $data);
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit_disable_id_data_siswa($id)
    {
        $data_ortu = dataOrtu::find($id);
        // $data_ortu = dataOrtu::where('id_data_ortu',$id)->first();
        $data['data_ortu'] = $data_ortu;
        $data_siswa = dataSiswa::all();
        $data['data_siswa'] = $data_siswa;
        return view('data_ortu.ubah_disable_id_data_siswa', $data);
        //bikin disable data siswa
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_ortu = dataOrtu::find($id);
        $data_ortu->id_data_siswa = $request->id_data_siswa;
        $data_ortu->nama_ayah = $request->nama_ayah;
        $data_ortu->pekerjaan_ayah = $request->pekerjaan_ayah;
        $data_ortu->nama_ibu = $request->nama_ibu;
        $data_ortu->pekerjaan_ibu = $request->pekerjaan_ibu;
        $data_ortu->save();
        echo "tersimpan";
        // redirect ke /ortu/{id}/edit
        return redirect('/siswa')->with('pesan','tersimpan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_ortu = dataOrtu::find($id);
        $data_ortu->delete();
        echo "terhapus";
        return redirect('/ortu')->with('pesan','terhapus');
    }
}
