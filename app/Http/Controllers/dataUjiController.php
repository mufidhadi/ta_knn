<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\dataSiswa;
use App\data_uji;

class dataUjiController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_uji = data_uji::all();
        $data['data_uji'] = $data_uji;
        return view('data_uji.daftar', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id_data_siswa = Input::get('siswa');
        $data['spesifik'] = false;
        if($id_data_siswa!=null){
            $data_siswa = dataSiswa::find($id_data_siswa);
            $data['spesifik'] = true;
        }else{
            $data_siswa = dataSiswa::all();
        }
        $data['data_siswa'] = $data_siswa;
        return view('data_uji.tambah',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data_uji = new data_uji;
        $data_uji->id_data_siswa = $request->id_data_siswa;
        $data_uji->penghasilan_ayah = $request->penghasilan_ayah;
        $data_uji->penghasilan_ibu = $request->penghasilan_ibu;
        $data_uji->tanggungan = $request->tanggungan;
        $data_uji->nilai_raport = $request->nilai_raport;
        $data_uji->kps = $request->kps;
        $data_uji->save();
        // return redirect('/uji')->with('pesan','tersimpan');
        return redirect('/hitung/uji/create')->with('pesan','tersimpan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_uji = data_uji::find($id);
        $data['data_uji'] = $data_uji;
        $data_siswa = dataSiswa::all();
        $data['data_siswa'] = $data_siswa;
        return view('data_uji.ubah',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_uji = data_uji::find($id);
        $data_uji->id_data_siswa = $request->id_data_siswa;
        $data_uji->penghasilan_ayah = $request->penghasilan_ayah;
        $data_uji->penghasilan_ibu = $request->penghasilan_ibu;
        $data_uji->tanggungan = $request->tanggungan;
        $data_uji->nilai_raport = $request->nilai_raport;
        $data_uji->kps = $request->kps;
        $data_uji->save();
        return redirect('/uji')->with('pesan','tersimpan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_uji = data_uji::find($id);
        $data_uji->delete();
        return redirect('/uji')->with('pesan','terhapus');
    }
}
