<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Input;
use App\dataSiswa;
use App\data_latih;

class dataLatihController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data_latih = data_latih::all();
        $data['data_latih'] = $data_latih;
        return view('data_latih.daftar', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $id_data_siswa = Input::get('siswa');
        $data['spesifik'] = false;
        if($id_data_siswa!=null){
            $data_siswa = dataSiswa::find($id_data_siswa);
            $data['spesifik'] = true;
        }else{
            $data_siswa = dataSiswa::all();
        }
        $data['data_siswa'] = $data_siswa;
        return view('data_latih.tambah',$data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data_latih = new data_latih;
        $data_latih->id_data_siswa = $request->id_data_siswa;
        $data_latih->penghasilan_ayah = $request->penghasilan_ayah;
        $data_latih->penghasilan_ibu = $request->penghasilan_ibu;
        $data_latih->tanggungan = $request->tanggungan;
        $data_latih->nilai_raport = $request->nilai_raport;
        $data_latih->kps = $request->kps;
        $data_latih->beasiswa = $request->beasiswa;
        $data_latih->save();
        // return redirect('/latih')->with('pesan','tersimpan');
        return redirect('/hitung/latih/create')->with('pesan','tersimpan');
    }
    
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data_latih = data_latih::find($id);
        $data['data_latih'] = $data_latih;
        $data_siswa = dataSiswa::all();
        $data['data_siswa'] = $data_siswa;
        return view('data_latih.ubah',$data);
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data_latih = data_latih::find($id);
        $data_latih->id_data_siswa = $request->id_data_siswa;
        $data_latih->penghasilan_ayah = $request->penghasilan_ayah;
        $data_latih->penghasilan_ibu = $request->penghasilan_ibu;
        $data_latih->tanggungan = $request->tanggungan;
        $data_latih->nilai_raport = $request->nilai_raport;
        $data_latih->kps = $request->kps;
        $data_latih->beasiswa = $request->beasiswa;
        $data_latih->save();
        return redirect('/latih')->with('pesan','tersimpan');
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $data_latih = data_latih::find($id);
        $data_latih->delete();
        return redirect('/latih')->with('pesan','terhapus');
    }
}
