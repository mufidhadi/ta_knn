<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class detail_hitung extends Model
{
    protected $table = 'detail_hitung';
    protected $primaryKey = 'id_detail_hitung';
    public $timestamps = false;
    
    public function detailHitung()
    {
        return $this->belongsTo('App\detailHitung', 'id_hitung', 'id_hitung');
    }
    public function dataLatih()
    {
        return $this->belongsTo('App\data_latih', 'id_data_latih', 'id_data_latih');
    }
}
