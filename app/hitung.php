<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class hitung extends Model
{
    protected $table = 'hitung';
    protected $primaryKey = 'id_hitung';
    public $timestamps = false;
    protected $appends = ['data_siswa'];
    
    public function dataUji()
    {
        return $this->belongsTo('App\data_uji', 'id_data_uji', 'id_data_uji');

    }
    public function dataSiswa(){
        return $this->dataUji->dataSiswa;
    }
}
