<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_uji extends Model
{
    protected $table = 'data_uji';
    protected $primaryKey = 'id_data_uji';
    public $timestamps = false;
    
    public function dataSiswa()
    {
        return $this->belongsTo('App\dataSiswa', 'id_data_siswa', 'id_data_siswa');
    }
}
