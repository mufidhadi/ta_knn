<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class data_latih extends Model
{
    protected $table = 'data_latih';
    protected $primaryKey = 'id_data_latih';
    public $timestamps = false;
    
    public function dataSiswa()
    {
        return $this->belongsTo('App\dataSiswa', 'id_data_siswa', 'id_data_siswa');
    }
}
