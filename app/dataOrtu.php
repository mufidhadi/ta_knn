<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class dataOrtu extends Model
{
    protected $table = 'data_ortu';
    protected $primaryKey = 'id_data_ortu';
    public $timestamps = false;
    
    public function dataSiswa()
    {
        return $this->belongsTo('App\dataSiswa', 'id_data_siswa', 'id_data_siswa');
    }
}
