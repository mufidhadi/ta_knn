<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index');

// proses input
Route::resource('siswa','dataSiswaController');
Route::resource('ortu','dataOrtuController');
Route::get('ortu/create/{id_data_siswa}', 'dataOrtuController@create_with_id_data_siswa');
Route::get('ortu/{id_data_ortu}/edit_siswa', 'dataOrtuController@edit_disable_id_data_siswa');
Route::resource('latih','dataLatihController');
Route::resource('uji','dataUjiController');
// proses hitung dari data uji
Route::resource('hitung/uji','hitungByUjiController');
Route::get('uji/euclidean/{id_hitung}','hitungByUjiController@euclidean_distance');
Route::get('uji/order/{id_hitung}','hitungByUjiController@order_k');
Route::get('uji/conclution/{id_hitung}','hitungByUjiController@conclution');
Route::post('add_to_latih/{id_hitung}','hitungByUjiController@add_to_latih');
// proses hitung dari data latih
Route::resource('hitung/latih','hitungByLatihController');
Route::get('latih/euclidean/{id_hitung}','hitungByLatihController@euclidean_distance');
Route::get('latih/order/{id_hitung}','hitungByLatihController@order_k');
Route::get('latih/conclution/{id_hitung}','hitungByLatihController@conclution');

Route::get('tes',function(){
    echo '<form method="post">';
    for ($i=0; $i < 10; $i++) { 
        echo 'ini nih-'.$i.'<input type="checkbox" name="ini[]" value="ini-'.$i.'"><br>';
    }
    echo '<input name="_token" value="'.csrf_token().'"><br>';
    echo '<button>';
    echo 'oke!';
    echo '</button>';
    echo '</form>';
});

Route::post('tes',function(){
    var_dump($_POST);
});
