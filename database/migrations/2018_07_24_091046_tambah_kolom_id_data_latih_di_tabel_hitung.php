<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class TambahKolomIdDataLatihDiTabelHitung extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::table('hitung',function(Blueprint $table){
            $table->integer('id_data_latih')->after('id_data_uji')->nullable();
        });
    }
    
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::table('hitung',function(Blueprint $table){
            $table->dropColumn('id_data_latih');
        });
    }
}
