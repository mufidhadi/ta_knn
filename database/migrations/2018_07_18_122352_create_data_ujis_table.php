<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDataUjisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('data_uji', function (Blueprint $table) {
            $table->increments('id_data_uji');
            $table->integer('id_data_siswa');
            // $table->foreign('id_data_siswa')->references('id_data_siswa')->on('data_siswa');
            $table->integer('penghasilan_ayah');
            $table->integer('penghasilan_ibu');
            $table->integer('tanggungan');
            $table->integer('nilai_raport');
            $table->enum('kps',['ya','tidak']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('data_uji');
    }
}
