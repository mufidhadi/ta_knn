<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDetailHitungsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('detail_hitung', function (Blueprint $table) {
            $table->increments('id_detail_hitung');
            $table->integer('id_hitung');
            // $table->foreign('id_hitung')->references('id_hitung')->on('hitung');
            $table->integer('id_data_latih');
            // $table->foreign('id_data_latih')->references('id_data_latih')->on('data_latih');
            $table->float('jarak_euclidean',15,2)->nullable();
            $table->integer('urutan')->nullable();
            $table->enum('anggota_k',['ya','tidak'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('detail_hitung');
    }
}
