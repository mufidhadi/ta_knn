<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateHitungsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('hitung', function (Blueprint $table) {
            $table->increments('id_hitung');
            $table->integer('id_data_uji')->nullable();
            // $table->foreign('id_data_uji')->references('id_data_uji')->on('data_uji');
            $table->integer('jumlah_k');
            $table->enum('kesimpulan_beasiswa',['ya','tidak'])->nullable();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('hitung');
    }
}
